'use strict';

const { Command } = require('./separators');
const requires = require('../core/requires');

/**
 * Checks if data has a usable command
 * (false even if it has a command but the username does not equal to the bot's)
 */
exports.command = {
  label: 'Command',
  requires: requires.has('message', 'text'),
  callback: (data, params, bot) => {
    let command = new Command(data.message.text);
    console.log(command);
    if (!command.valid || command.bot && bot.me.username !== command.bot) {
      // command is not valid or not meant to be handled by this bot
      return false;
    } else {
      // command is valid and should be handled by this bot
      // write command into data
      params.command = command;
      return true;
    }
  }
};

/**
 * Checks if command equals to given name
 * @param  {String|String[]} name    The command or array of possible commands
 * @param  {object}          options
 * @return {boolean}
 */
exports.command.is = (name, { caseSensitive, noTrim } = {}) => {
  return {
    label: `Is command '${name}'`,
    requires: exports.command,
    callback: (data, params, bot) => {
      // put name into array if isn't already
      if (typeof name === 'string') {
        name = [name];
      }
      // trim and lowercase every name
      for (let i = 0; i < name.length; i++) {
        name[i] = noTrim ? name[i] : name[i].trim();
        name[i] = caseSensitive ? name[i] : name[i].toLowerCase();
      }
      let command = params.command.command;
      command = noTrim ? command : command.trim();
      command = caseSensitive ? command : command.toLowerCase();
      if (name.includes(command)) {
        return true;
      } else {
        return false;
      }
    }
  };
};
