'use strict';

/**
 * Makes bot Telegram-ready
 * @param {Bot} bot
 */
module.exports.init = (bot) => {
  bot.identify = () => {
    bot.send('getMe', {}, (error, response, body) => {
      if(error) {
        console.error(error);
      }
      this.me = body.ok ? body.result : null;
      console.log('Me', this.me);
    });
  };
  bot.identify();
};

module.exports.requires = require('./requires');
module.exports.separators = require('./separators');
