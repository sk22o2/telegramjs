'use strict';

const defaults = require('./defaults'),
      request = require('request'),
      url = require('url');

class Bot {

  /**
   * Handles the data received from the API
   * @param  {object} data The data supplied by the API
   */
  handle(data) {
    console.log('Handling', data);
    console.log('Handlers', this.handlers);

    let requirements = [];
    /** @param object Object that might have `requires` node */
    let getRequirements = (object) => {
      if(object.requires) {
        let requires = object.requires instanceof Array ?
                       object.requires : [ object.requires ];

        requires.forEach((requirement) => {
          // if may be required multiple times or
          // has not been added to the requirements until then
          if((requirement.options && requirement.options.multiple) ||
            !requirements.includes(requirement)) {
            // recursively get requirements of requirements
            getRequirements(requirement);
            requirements.push(requirement);
          }
        });
      }
    };

    /**
     * Loops through all handlers, calls the requirements and the handlers
     * themselves if requirements fit
     */
    this.handlers.forEach((handler) => {
      // handler: { callback, requires }
      requirements = [];
      getRequirements(handler);
      if(requirements.length) {
        let params = {};
        for (let i = 0; i < requirements.length; i++) {
          let req = requirements[i];
          let result = req.callback(data, params, this);
          if (!result) {
            break;
          }
          if (i+1 === requirements.length) {
            handler.callback(data, params, this);
          }
        }
      }
    });
  }

  /**
   * Receives HTTP requests from the API
   * @param req Request
   * @param res Response
   */
  request(req, res) {
    if (req.method === 'POST') {
      var parts = url.parse(req.url, true);
      if(parts.query && parts.query.url && this.url === parts.query.url) {
        var body = '';
        req.on('data', chunk => body+=chunk);
        req.on('end', () => {
          res.writeHead(200, {'Content-Type': 'text/plain'});
          res.end(this.handle(JSON.parse(body)));
        });
      } else {
        res.writeHead(403, {'Content-Type': 'text/plain'});
        res.end('Given URL did not match with API_URL');
      }
    } else {
      res.writeHead(400, {'Content-Type': 'text/plain'});
      res.end('Nothing to do here!');
    }
  }

  /**
   * @callback requirement[] Modifies or checks the given data, which is then
   *           							 passed to the next requirement in the array.
   *           							 Must return either true or false. If false is
   *           							 returned, the handler will not be executed at all,
   *           							 as all requirements need to return true.
   * @param {object} data    The modifiable data
   * @param {Bot} bot        The bot object. Can for example be used to check
   *                         if the bot's identity (bot.me) matches with what's
   *                         required. (e.g. the bot's username and the
   *                         command's bot username)
   */

  /**
   * Register handler
   * @param {function|object} callback   Callback function or handler object.
   *                                     `requirements` is not needed if handler
   *                                     object is given. Callback will be
   *                                     called if all requirements return true
   * @param {requirement[]}   [requires] Used to make changes to the the data
   *                                     and check if it fits the requirements
   * @param {String}          [label]    Handler's name
   */
  register(callback, requires, label) {
    let handler = {};
    if (typeof callback === 'object') {
      handler = callback;
    } else {
      handler = { callback, requires, label };
    }
    console.log("Registering", (handler.label || handler.callback));
    this.handlers.push(handler);
  }

  /**
   * @callback response
   * @param {object} error
   * @param {object} response
   * @param {String} body
   */

  /**
   * @param {string}   method
   * @param {object}   update
   * @param {response} callback Callback containing the API's response data
   */
  send(method, update, callback) {
    request({
      url: this.url+method,
      method: 'POST',
      json: update
    }, callback);
  }

  /**
   * @param {String} url URL to the bot's API
   */

  constructor(url) {
    this.url = url;
    this.handlers = defaults.handlers;
    require('http')
      .createServer((req, res) => this.request(req, res))
      .listen(process.env.NODE_PORT || 3000, process.env.NODE_IP || 'localhost',
      () => console.log(`Application worker ${process.pid} started...`));
  }
}

module.exports = Bot;
